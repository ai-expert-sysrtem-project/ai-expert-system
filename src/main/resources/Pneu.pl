% Author: Oraine
% Author: Shakeem

% Date: 10/22/2019


%structure of facts factorcategory(factor,weighting value)

 %FACTS factor ----1
 riskcountry('south asia').
 riskcountry('sub-Saharan Africa').

 %life style factors ----factor 2
 contributing_lifestyle_factor('excessive drinking').
 contributing_lifestyle_factor('cigarette smoking').
 contributing_lifestyle_factor(malnourishment).
 contributing_lifestyle_factor('poor hygiene conditions').
 contributing_lifestyle_factor('inadequate rest').
 contributing_lifestyle_factor('lack of exercise').

 %Environmental Factors factor---3
 environmental_factor('chemical pollutants').

 %underlying medical conditions factor-----4
 contributing_medical_condition(stroke).
 contributing_medical_condition('cystic fibrosis').
 contributing_medical_condition(asthma).
 contributing_medical_condition('COPD').
 contributing_medical_condition(bronchiectasis).
 contributing_medical_condition(diabetis).
 contributing_medical_condition('heart failure').
 contributing_medical_condition('sickle cell disease').
 contributing_medical_condition('HIV/AIDS').


 %uncommon factors factor----5
 contributing_uncommon_factor(ventillator).
 contributing_uncommon_factor('blood transplant').
 contributing_uncommon_factor('bone marrow transplant').
 contributing_uncommon_factor('stem cell transplant').
 contributing_uncommon_factor(chemotherapy).
 contributing_uncommon_factor('long term steroid usage').

 % vaccination factor factor----6
 factor(vaccination).

 %RULES...........................................................
% expert_system:-write('do you have HIV?'),nl,read(response),nl,val1 is
% 3,nl,
  %  display(response,val1).

%med(val1):- contributing_medical_condition('AIDS').

%display(response,val1):-write('the value of val1 is:').

%(response == y-> val1 is 3; val1 is 0)
 %................................................................



expert_system:-  nl,write('Please state your name?'), nl,read(Name),

                  nl,write('Please enter your temperature in degrees celcius:'),nl,read(Tem),

                   nl,write('Have you visited South Asia or sub-Saharan Africa recently?'), nl,read(Cntyresp1),
                  (Cntyresp1 == y -> Factor1 is 1; Factor1 is 0),
                  %...................................................................................................................................
                   nl,write('Do you drink excessively'), nl,read(Cntyresp2),
                   (Cntyresp2 == y -> Factor2A is 1; Factor2A is 0),

                    nl,write('Do you smoke cigarettes?'), nl,read(Cntyresp3),
                   (Cntyresp3 == y -> Factor2b is 3; Factor2b is 0),

                    nl,write('Are you Malnourished?'), nl,read(Cntyresp4),
                   (Cntyresp4 == y -> Factor2c is 2; Factor2c is 0),%....

                    nl,write('Do you practice proper personal hygiene? or is your environment\'s condition condusive to personal hygiene?'), nl,read(Cntyresp5),
                   (Cntyresp5 == n -> Factor2d is 3; Factor2d is 0),%....

                    nl,write('Do you get enough rest?'), nl,read(Cntyresp6),
                   (Cntyresp6 == n -> Factor2e is 2; Factor2e is 0),%....

                    nl,write('Do you engage in regular physical activity or exercise?'), nl,read(Cntyresp7),
                   (Cntyresp7 == n -> Factor2f is 2; Factor2f is 0),%....
                   %.................................................................................................................................
                    nl,write('Does your environment contain toxic fumes or potentialy hazardous chemicals?'), nl,read(Cntyresp8),
                   (Cntyresp8 == y -> Factor3a is 1.0; Factor3a is 0),%....

                   %.................................................................................................................................
                    nl,write('Do you suffer from stroke or any medical condition that renders you immobile?'), nl,read(Cntyresp9),
                   (Cntyresp9 == y -> Factor4a is 0.001; Factor4a is 0),%....


                    nl,write('Do you have Cystic Fibrosis'), nl,read(Cntyresp10),
                   (Cntyresp10 == y -> Factor4b is 0.001; Factor4b is 0),%....


                    nl,write('Do you have Asthma?'), nl,read(Cntyresp11),
                   (Cntyresp11 == y -> Factor4c is 0.2; Factor4c is 0),%....


                    nl,write('Do you suffer from Chronic Obstructive Pulmonary Disease (COPD)?'), nl,read(Cntyresp12),
                   (Cntyresp12 == y -> Factor4d is 0.2; Factor4d is 0),%....


                    nl,write('Do you suffer from bronchiectasis ?'), nl,read(Cntyresp13),
                   (Cntyresp13 == y -> Factor4e is 0.2; Factor4e is 0),%....


                    nl,write('Are you living with diabetis?'), nl,read(Cntyresp14),
                   (Cntyresp14 == y -> Factor4f is 0.033; Factor4f is 0),%....


                    nl,write('Are you plagued with heart faulure?'), nl,read(Cntyresp15),
                   (Cntyresp15 == y -> Factor4g is 0.033; Factor4g is 0),%....


                    nl,write('Do you have sickle cell disease?'), nl,read(Cntyresp16),
                   (Cntyresp16 == y -> Factor4h is 0.033; Factor4h is 0),%....


                    nl,write('Do you have HIV/AIDS?'), nl,read(Cntyresp17),
                   (Cntyresp17 == y -> Factor4i is 0.2; Factor4i is 0),%....

                   %..........................................................................................................
                    nl,write('Do you rely on a ventillator or any other such device to aid in breathing?'), nl,read(Cntyresp18),
                   (Cntyresp18 == y -> Factor5a is 0.3; Factor5a is 0),%....

                    nl,write('Have you recently underwent any tissue transplant (blood,bone marrow,stem cells etc)?'), nl,read(Cntyresp19),
                   (Cntyresp19 == y -> Factor5b is 0.2; Factor5b is 0),%....

                    nl,write('Have you recently underwent chemotherapy?'), nl,read(Cntyresp20),
                   (Cntyresp20 == y -> Factor5c is 0.3; Factor5c is 0),%....

                    nl,write('Have you been taking steroids over an extended period of time?'), nl,read(Cntyresp21),
                   (Cntyresp21 == y -> Factor5d is 0.2; Factor5d is 0),%....
                   %..........................................................................................................
                    nl,write('Are you Vaccinated for Pneumonia?'), nl,read(Cntyresp22),
                   (Cntyresp22 == y -> Factor6a is 1.0; Factor6a is 0),%....

                    nl,write('Are you between the ages of 2 and 65?'), nl,read(Cntyresp23),
                   (Cntyresp23 == y -> Factor7a is 0.5; Factor7a is 0),

                    det_risk_level(Country_risk_level,Lifestyle_risk_level,Environmental_risk_level,Medical_risk_level,Uncommon_factors_risk_level,Vaccination_factor_level,Age_factor,Factor1,Factor2A,Factor2b,Factor2c,Factor2d,Factor2e,Factor2f,Factor3a,Factor4a,Factor4b,Factor4c,Factor4d,Factor4e,Factor4f,Factor4g,Factor4h,Factor4i,Factor5a,Factor5b,Factor5c,Factor5d,Factor6a,Factor7a),
                    Overall_risk_level is Country_risk_level+Lifestyle_risk_level+ Environmental_risk_level+ Medical_risk_level+ Uncommon_factors_risk_level+ Vaccination_factor_level+Age_factor,
                    Temp is (Tem * 9/5) + 32,
                  display(Name,Country_risk_level,Lifestyle_risk_level,Environmental_risk_level,Medical_risk_level,Uncommon_factors_risk_level,Vaccination_factor_level,Overall_risk_level,Temp),

                  runagain.

 det_risk_level(Country_risk_level,Lifestyle_risk_level,Environmental_risk_level,Medical_risk_level,Uncommon_factors_risk_level,Vaccination_factor_level,Age_factor,Factor1,Factor2A,Factor2b,Factor2c,Factor2d,Factor2e,Factor2f,Factor3a,Factor4a,Factor4b,Factor4c,Factor4d,Factor4e,Factor4f,Factor4g,Factor4h,Factor4i,Factor5a,Factor5b,Factor5c,Factor5d,Factor6a,Factor7a):-

                  Country_risk_level is  5*(Factor1),
                  Lifestyle_risk_level is 10*(Factor2A+Factor2b+Factor2c+Factor2d+Factor2e+Factor2f),
                  Environmental_risk_level is 13*(Factor3a),
                  Medical_risk_level is 22*(Factor4a+Factor4b+Factor4c+Factor4d+Factor4e+Factor4f+Factor4g+Factor4h+Factor4i),
                  Uncommon_factors_risk_level is 18*(Factor5a+Factor5b+Factor5c+Factor5d),
                  Vaccination_factor_level is 27*(Factor6a),
                  Age_factor is 5*(Factor7a).


display(Name,Country_risk_level,Lifestyle_risk_level,Environmental_risk_level,Medical_risk_level,Uncommon_factors_risk_level,Vaccination_factor_level,Overall_risk_level,Temp) :-
                                                nl,write('*********RISK ASSESMENT REPORT********'),
				                nl, write('Name:'), write(Name),
				                nl, write('Country risk level: '), write(Country_risk_level),write(' %'),
				                nl, write('lifestyle risk level: '), write(Lifestyle_risk_level),write(' %'),
				                nl, write('environmental risk level: '), write(Environmental_risk_level),write(' %'),
                                                nl, write('medical risk level: '), write( Medical_risk_level),write(' %'),
                                                nl, write('uncommon risk factors level: '), write(Uncommon_factors_risk_level),write(' %'),
                                                nl, write('vaccination risk factor level: '), write(Vaccination_factor_level),write(' %'),
                                                nl, write('age risk factor level: '), write(Vaccination_factor_level),write(' %'),
				                nl, write('**************************'),
				                nl, write('Overall risk level: '), write(Overall_risk_level),write(' %'),
                                                nl, write('Temperature in Farenheit is: '), write(Temp),write(' %'),
                                                nl, write('**************************').


 runagain :- nl,nl,write('Do you wish to run the program again ? '), nl,read(Ans),
	 (Ans == y -> expert_system; write('Thank you for using the program. ')).
