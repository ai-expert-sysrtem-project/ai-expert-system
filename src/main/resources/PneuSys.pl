%Authors:

%Oraine
%Shakeem

%End Authors*************************************


%Below are the facts:


 risk_factor('Jamaica',0.25).
 %risk_factor('Age',0.4).

 %risk_factor('excessive_drinking',0.2).
 risk_factor('cigarette_smoking',0.3).
 risk_factor('Malnourishment',0.25).
 risk_factor('poor_hygiene_conditions',0.3).
 %risk_factor('Inadequate_Rest',0.2).
 risk_factor('Lack_of_Exercise',0.2).


 risk_factor('Chemical_Pollutants',0.45).


 risk_factor('Stroke',0.3).
 risk_factor('Cystic_Fibrosis',0.2).
 risk_factor('Asthma',0.54).
 %risk_factor('COPD',0.6).
 risk_factor('Bronchiectasis',0.5).
 %risk_factor('Diabetis',0.2).
 %risk_factor('Heart_Failure',0.3).
 %risk_factor('Sickle_Cell_Disease',0.2).
 %risk_factor('HIV/AIDS',0.4).

 %risk_factor('Ventillator',0.35).
 %risk_factor('Blood_Transplant',0.3).
 risk_factor('Bone_Marrow_Transplant',0.4).
 %risk_factor('Stem_Cell_Transplant',0.4).
 %risk_factor('Chemotherapy',0.3).
 risk_factor('Long_Term_Steroid_Usage',0.1).

 %risk_factor('Vaccination',0.6).


% End Facts******************************************************************


%Predicates

single_risk_score(Who):-risk_factor(Who,X),write(X).

% risk_total().

risk_total([],0).
risk_total([Risk|More],X) :- risk_total(More,Score), risk_factor(Risk,RiskScore),X is RiskScore + Score.
