%Authors:

%Oraine
%Shakeem

%End Authors*************************************


%Below are the facts:


 risk_factor('Jamaica',0.25).
 risk_factor('Age',0.4).

 risk_factor('excessive drinking',0.2).
 risk_factor('cigarette smoking',0.3).
 risk_factor('Malnourishment',0.25).
 risk_factor('poor hygiene conditions',0.3).
 risk_factor('Inadequate Rest',0.2).
 risk_factor('Lack of Exercise',0.2).


 risk_factor('Chemical Pollutants',0.45).


 risk_factor('Stroke',0.3).
 risk_factor('Cystic Fibrosis',0.2).
 risk_factor('Asthma',0.54).
 risk_factor('COPD',0.6).
 risk_factor('Bronchiectasis',0.5).
 risk_factor('Diabetis',0.2).
 risk_factor('Heart Failure',0.3).
 risk_factor('Sickle Cell Disease',0.2).
 risk_factor('HIV/AIDS',0.4).

 risk_factor('Ventillator',0.35).
 risk_factor('Blood Transplant',0.3).
 risk_factor('Bone Marrow Transplant',0.4).
 risk_factor('Stem Cell Transplant',0.4).
 risk_factor('Chemotherapy',0.3).
 risk_factor('Long Term Steroid Usage',0.1).

 risk_factor('Vaccination',0.6).


% End Facts******************************************************************


%Predicates

single_risk_score(Who):-risk_factor(Who,X),write(X).

risk_total().

risk_total([],0).
risk_total([Risk|More],X) :- risk_total(More,Score), risk_factor(Risk,RiskScore),
X is RiskScore * Score.
