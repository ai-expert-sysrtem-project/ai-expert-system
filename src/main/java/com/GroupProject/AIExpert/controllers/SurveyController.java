package com.GroupProject.AIExpert.controllers;

import com.GroupProject.AIExpert.repositories.IAlertRepository;
import com.GroupProject.AIExpert.repositories.ISurveyRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.GroupProject.AIExpert.exception.ResourceNotFoundException;
import com.GroupProject.AIExpert.models.QueryResult;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import com.GroupProject.AIExpert.services.SynchronousQuery;
import com.GroupProject.AIExpert.entities.Alert;
import com.GroupProject.AIExpert.entities.Survey;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/survey")
public class SurveyController {

    
    private ISurveyRepository surveyRepository;
    private IAlertRepository alertRepository;


    @Autowired
    private JavaMailSender javaMailSender;

    public SurveyController(ISurveyRepository surveyRepository,IAlertRepository alertRepository) {
        this.surveyRepository = surveyRepository;
        this.alertRepository = alertRepository;
    }

    

    @GetMapping()
    public List<Survey> getAllSurveys() {

        return surveyRepository.findAll();
    }

    @PostMapping()
    public QueryResult create(@Valid @RequestBody Survey survey) {
        survey.setTemp((survey.getTemp() - 32) * 5/9);

        surveyRepository.save(survey);

        SynchronousQuery queryRunner = new SynchronousQuery();
        QueryResult result = queryRunner.runQuery(survey.toString());
        this.sendEmail("pneumoniaexpert@gmail.com","Query Result","The result recorded is "  + result.VariableValues.get(0));


        return result;
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete (@PathVariable(value= "id") long id){

        Survey findSurvey;
        
        findSurvey = surveyRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Survey","id",id));

        surveyRepository.deleteById(findSurvey.getId());
        return ResponseEntity.ok().build();
    }

    public Boolean sendEmail(String To,String Subject,String message) {

        try {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(To);

            msg.setSubject(Subject);
            msg.setText(message);

            this.javaMailSender.send(msg);
            Alert alert = new Alert(To,Subject,message);
            alertRepository.save(alert);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @GetMapping("/emailtest")
    public String EmailTest() {

        // EmailSender emailsender = new EmailSender();

        
        if (this.sendEmail("pneumoniaexpert@gmail.com@gmail.com","Test Subject","Test message"))
        {
            Alert newAlert = new Alert("TroyAnderson.d@gmail.com", "Test message", "Test Subject");
            alertRepository.save(newAlert);
            return "Sucessfully Sent";
        }
        return "Nope";
    }

}