
package com.GroupProject.AIExpert.controllers;

import java.io.UnsupportedEncodingException;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.GroupProject.AIExpert.services.SynchronousQuery;
import com.GroupProject.AIExpert.models.QueryResult;
import com.GroupProject.AIExpert.services.EmailSender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class QueryController {
    

    @Autowired
    private JavaMailSender javaMailSender;

    public Boolean sendEmail(String To,String Subject,String message) {

        try {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(To);

            msg.setSubject(Subject);
            msg.setText(message);

            this.javaMailSender.send(msg);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    @GetMapping("/test")
    public ResponseEntity<QueryResult> getString() {

    try{
        SynchronousQuery test = new SynchronousQuery();
        String QueryString = "risk_total(['Stroke','Asthma','Bronchiectasis'],X)";
        // String QueryString = "risk_total(['Stroke','Asthma'],X)";
        // String QueryString = "contributing_medical_condition(Conditions)";
        QueryResult result = test.runQuery(QueryString);
        return new ResponseEntity<QueryResult>(result,HttpStatus.OK);
    }catch(Exception ex){
        ex.printStackTrace();
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}

    @PostMapping("/query")
    public QueryResult QueryKnowledgeBase(@Valid @RequestBody String query) {

        // query = query.replaceAll("+", " ");
        query = query.replaceAll("%27", "'");
        query = query.replaceAll("%2F", "/");
        query = query.replaceFirst("%28", "(");
        query = query.replaceFirst("%29=", ")");
        query = query.replaceFirst("%2C", ",");

        this.sendEmail("TroyAnderson.d@gmail.com","Your Query",query);


        System.out.println(query);
        SynchronousQuery queryRunner = new SynchronousQuery();
        return queryRunner.runQuery(query);
    }

   
}