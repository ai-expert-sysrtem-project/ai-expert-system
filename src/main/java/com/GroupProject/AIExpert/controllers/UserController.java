package com.GroupProject.AIExpert.controllers;

import java.util.List;

import com.GroupProject.AIExpert.entities.ApplicationUser;
import com.GroupProject.AIExpert.repositories.IApplicationUserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.GroupProject.AIExpert.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/users")
public class UserController {

    private IApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(IApplicationUserRepository applicationUserRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping()
    public List<ApplicationUser> getAllUsers() {

        return applicationUserRepository.findAll();
    }

    @PostMapping("/record")
    public ResponseEntity<ApplicationUser> signUp(@RequestBody ApplicationUser user) {

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
        user.setPassword("HIDDEN");
        return new ResponseEntity<ApplicationUser>(user, HttpStatus.CREATED);

    }

    @PostMapping("/login")
    public ResponseEntity<ApplicationUser> Login(@RequestBody ApplicationUser user) {

        ApplicationUser finduser = applicationUserRepository.findByUsername(user.getUsername());

        if (finduser == null) {
            System.out.println("-------------------------------");
            System.out.println("Find user is null");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!bCryptPasswordEncoder.matches(user.getPassword(), finduser.getPassword())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        finduser.setPassword("HIDDEN");
        return new ResponseEntity<ApplicationUser>(finduser, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ApplicationUser> delete(@PathVariable(value = "id") long id) {

        ApplicationUser findUser;

        findUser = applicationUserRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Survey", "id", id));

        applicationUserRepository.deleteById(findUser.getId());
        return new ResponseEntity<ApplicationUser>(findUser, HttpStatus.OK);

    }
}