package com.GroupProject.AIExpert.controllers;

import java.util.List;

import com.GroupProject.AIExpert.entities.Alert;
import com.GroupProject.AIExpert.repositories.IAlertRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.GroupProject.AIExpert.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/alert")
public class AlertController {

    private IAlertRepository alertRepository;

    public AlertController(IAlertRepository alertRepository) {
        this.alertRepository = alertRepository;
    }

    @GetMapping()
    public List<Alert> getAllUsers() {

        return alertRepository.findAll();
    }

    @PostMapping()
    public ResponseEntity<Alert> create(@RequestBody Alert alert) {

        System.out.println(alert.getMessage());
        System.out.println(alert.getSubject());
        System.out.println(alert.getReciever());
        System.out.println("---------------------------------------------");
        alertRepository.save(alert);
        return new ResponseEntity<Alert>(alert, HttpStatus.CREATED);

    }

    @DeleteMapping("{id}")
    public ResponseEntity<Alert> delete(@PathVariable(value = "id") long id) {

        Alert FindAlert;

        FindAlert = alertRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Alert", "id", id));

                alertRepository.deleteById(FindAlert.getId());
        return new ResponseEntity<Alert>(FindAlert, HttpStatus.OK);

    }
}