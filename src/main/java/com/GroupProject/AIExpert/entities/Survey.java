package com.GroupProject.AIExpert.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Survey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double temp;
    // private Boolean jamaican;

    // private Boolean excessive_drinking;
    private Boolean cigarette_smoking;
    private Boolean Malnourishment;
    private Boolean poor_hygiene_conditions;
    // private Boolean Inadequate_Rest;
    private Boolean Lack_of_Exercise;
    private Boolean Chemical_Pollutants;

    private Boolean Stroke;
    private Boolean Cystic_Fibrosis; 
    private Boolean Asthma; 
    // private Boolean COPD; 
    private Boolean Bronchiectasis; 
    // private Boolean Diabetis; 
    // private Boolean Heart_Failure; 
    // private Boolean Sickle_Cell_Disease;
    // private Boolean HIV_AIDS;


    // private Boolean Ventillator;
    // private Boolean Blood_Transplant;
    private Boolean Bone_Marrow_Transplant;
    // private Boolean Stem_Cell_Transplant;
    // private Boolean Chemotherapy;
    private Boolean Long_Term_Steroid_Usage;
    // private Boolean Vaccination;


   

    public Survey() {
    }


    public Survey(double temp, Boolean cigarette_smoking, Boolean Malnourishment, Boolean poor_hygiene_conditions, Boolean Lack_of_Exercise, Boolean Chemical_Pollutants, Boolean Stroke, Boolean Cystic_Fibrosis, Boolean Asthma, Boolean Bronchiectasis, Boolean Bone_Marrow_Transplant, Boolean Long_Term_Steroid_Usage) {
        this.temp = temp;
        this.cigarette_smoking = cigarette_smoking;
        this.Malnourishment = Malnourishment;
        this.poor_hygiene_conditions = poor_hygiene_conditions;
        this.Lack_of_Exercise = Lack_of_Exercise;
        this.Chemical_Pollutants = Chemical_Pollutants;
        this.Stroke = Stroke;
        this.Cystic_Fibrosis = Cystic_Fibrosis;
        this.Asthma = Asthma;
        this.Bronchiectasis = Bronchiectasis;
        this.Bone_Marrow_Transplant = Bone_Marrow_Transplant;
        this.Long_Term_Steroid_Usage = Long_Term_Steroid_Usage;
    }



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTemp() {
        return this.temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public Boolean isCigarette_smoking() {
        return this.cigarette_smoking;
    }

    public Boolean getCigarette_smoking() {
        return this.cigarette_smoking;
    }

    public void setCigarette_smoking(Boolean cigarette_smoking) {
        this.cigarette_smoking = cigarette_smoking;
    }

    public Boolean isMalnourishment() {
        return this.Malnourishment;
    }

    public Boolean getMalnourishment() {
        return this.Malnourishment;
    }

    public void setMalnourishment(Boolean Malnourishment) {
        this.Malnourishment = Malnourishment;
    }

    public Boolean isPoor_hygiene_conditions() {
        return this.poor_hygiene_conditions;
    }

    public Boolean getPoor_hygiene_conditions() {
        return this.poor_hygiene_conditions;
    }

    public void setPoor_hygiene_conditions(Boolean poor_hygiene_conditions) {
        this.poor_hygiene_conditions = poor_hygiene_conditions;
    }

    public Boolean isLack_of_Exercise() {
        return this.Lack_of_Exercise;
    }

    public Boolean getLack_of_Exercise() {
        return this.Lack_of_Exercise;
    }

    public void setLack_of_Exercise(Boolean Lack_of_Exercise) {
        this.Lack_of_Exercise = Lack_of_Exercise;
    }

    public Boolean isChemical_Pollutants() {
        return this.Chemical_Pollutants;
    }

    public Boolean getChemical_Pollutants() {
        return this.Chemical_Pollutants;
    }

    public void setChemical_Pollutants(Boolean Chemical_Pollutants) {
        this.Chemical_Pollutants = Chemical_Pollutants;
    }

    public Boolean isStroke() {
        return this.Stroke;
    }

    public Boolean getStroke() {
        return this.Stroke;
    }

    public void setStroke(Boolean Stroke) {
        this.Stroke = Stroke;
    }

    public Boolean isCystic_Fibrosis() {
        return this.Cystic_Fibrosis;
    }

    public Boolean getCystic_Fibrosis() {
        return this.Cystic_Fibrosis;
    }

    public void setCystic_Fibrosis(Boolean Cystic_Fibrosis) {
        this.Cystic_Fibrosis = Cystic_Fibrosis;
    }

    public Boolean isAsthma() {
        return this.Asthma;
    }

    public Boolean getAsthma() {
        return this.Asthma;
    }

    public void setAsthma(Boolean Asthma) {
        this.Asthma = Asthma;
    }

    public Boolean isBronchiectasis() {
        return this.Bronchiectasis;
    }

    public Boolean getBronchiectasis() {
        return this.Bronchiectasis;
    }

    public void setBronchiectasis(Boolean Bronchiectasis) {
        this.Bronchiectasis = Bronchiectasis;
    }

    public Boolean isBone_Marrow_Transplant() {
        return this.Bone_Marrow_Transplant;
    }

    public Boolean getBone_Marrow_Transplant() {
        return this.Bone_Marrow_Transplant;
    }

    public void setBone_Marrow_Transplant(Boolean Bone_Marrow_Transplant) {
        this.Bone_Marrow_Transplant = Bone_Marrow_Transplant;
    }

    public Boolean isLong_Term_Steroid_Usage() {
        return this.Long_Term_Steroid_Usage;
    }

    public Boolean getLong_Term_Steroid_Usage() {
        return this.Long_Term_Steroid_Usage;
    }

    public void setLong_Term_Steroid_Usage(Boolean Long_Term_Steroid_Usage) {
        this.Long_Term_Steroid_Usage = Long_Term_Steroid_Usage;
    }


    @Override
    public String toString() {

        String query = "risk_total([";

        if(getCigarette_smoking())
            query+="'cigarette_smoking',";

        if(getMalnourishment())
            query+="'Malnourishment',";

        if(getPoor_hygiene_conditions())
            query+="'poor_hygiene_conditions',";

        if(getLack_of_Exercise())
            query+="'Lack_of_Exercise',";

        if(getChemical_Pollutants())
            query+="'Chemical_Pollutants',";

        if(getStroke())
            query+="'Stroke',";

        if(getCystic_Fibrosis())
            query+="'Cystic_Fibrosis',";

        if(getAsthma())
            query+="'Asthma',";

        if(getBronchiectasis())
            query+="'Bronchiectasis',";

        if(getBone_Marrow_Transplant())
            query+="'Bone_Marrow_Transplant',";
        
        if(getLong_Term_Steroid_Usage())
            query+="'Long_Term_Steroid_Usage'";

            query+= "],X)";

        if(!getLong_Term_Steroid_Usage())
            query = query.replace(",]", "]");
            
        return query;
    }




}