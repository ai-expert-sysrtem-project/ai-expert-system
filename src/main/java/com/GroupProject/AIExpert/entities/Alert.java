package com.GroupProject.AIExpert.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String reciever;
    public String message;
    public String subject;



    public Alert() {
    }


    public Alert(String reciever, String message, String subject) {
        this.reciever = reciever;
        this.message = message;
        this.subject = subject;
    }

    
	public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReciever() {
        return this.reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}