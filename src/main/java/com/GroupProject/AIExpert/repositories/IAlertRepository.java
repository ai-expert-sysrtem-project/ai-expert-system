package com.GroupProject.AIExpert.repositories;

import com.GroupProject.AIExpert.entities.Alert;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAlertRepository extends JpaRepository<Alert, Long> {
    
}