package com.GroupProject.AIExpert.repositories;

import com.GroupProject.AIExpert.entities.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
    ApplicationUser findByUsernameAndPassword(String username,String password);
}