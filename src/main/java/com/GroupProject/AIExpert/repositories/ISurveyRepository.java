package com.GroupProject.AIExpert.repositories;

import com.GroupProject.AIExpert.entities.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISurveyRepository extends JpaRepository<Survey, Long> {
    
}