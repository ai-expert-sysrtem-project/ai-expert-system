package com.GroupProject.AIExpert.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class EmailSender{

    @Autowired
    private JavaMailSender javaMailSender;

    public Boolean sendEmail() {

       try {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo("TroyAnderson.d@gmail.com");

            msg.setSubject("Testing from Spring Boot");
            msg.setText("Hello World \n Spring Boot Email");

            javaMailSender.send(msg);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}