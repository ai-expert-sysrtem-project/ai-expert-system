package com.GroupProject.AIExpert.services;

import com.GroupProject.AIExpert.models.QueryResult;
import com.ugos.jiprolog.engine.JIPEngine;
import com.ugos.jiprolog.engine.JIPQuery;
import com.ugos.jiprolog.engine.JIPSyntaxErrorException;
import com.ugos.jiprolog.engine.JIPTerm;
import com.ugos.jiprolog.engine.JIPVariable;

public class SynchronousQuery {

    public QueryResult runQuery(String QueryString) {
        // New instance of prolog engine
        JIPEngine jip = new JIPEngine();
        Boolean hasAllVariables = false;
        JIPTerm queryTerm = null;
        QueryResult queryResult = new QueryResult();

        queryResult.setQuery(QueryString);
        System.out.println(QueryString);

        // parse query
        try {
            // consult file
            // String filePath = "src/main/resources/prologdatabase/PneuSys.pl";
            String filePath = "src/main/resources/PneuSys.pl";
            jip.consultFile(filePath);

            queryTerm = jip.getTermParser().parseTerm(queryResult.getQuery());
        } catch (JIPSyntaxErrorException ex) {
            ex.printStackTrace();
            System.exit(0); // needed to close threads by AWT if shareware
        }

        // open Query
        JIPQuery jipQuery = jip.openSynchronousQuery(queryTerm);
        JIPTerm solution;
        // Loop while there is another solution
        while (jipQuery.hasMoreChoicePoints()) {
            solution = jipQuery.nextSolution();

            if (solution != null) {

                JIPVariable[] vars = solution.getVariables();

                for (JIPVariable var : vars) {

                    if (!var.isAnonymous()) {

                        System.out.println(var.getName() + " = " + var.toString(jip) + " ");
                        
                        if(!hasAllVariables)
                            queryResult.addVariableName(var.getName());

                        queryResult.addVariableValue(var.toString(jip));

                    }
                }
                hasAllVariables = true;
            }
        }

        return queryResult;
    }
}
