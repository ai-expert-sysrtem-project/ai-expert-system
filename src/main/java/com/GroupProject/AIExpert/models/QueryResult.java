package com.GroupProject.AIExpert.models;

import java.util.List;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;

public class QueryResult {

    @JsonProperty("query")
    public String query;

    @JsonProperty("variableNames")
    public List<String> VariableNames;

    @JsonProperty("variableValues")
    public List<String> VariableValues;

    public QueryResult() {
        this.query = new String("");
        this.VariableNames = new ArrayList<String>();
        this.VariableValues = new ArrayList<String>();
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String newquery) {
        this.query = newquery;
    }

    public List<String> getVariableNames() {
        return this.VariableNames;
    }

    private void setVariableNames(ArrayList<String> newVariables) {
        this.VariableNames = newVariables;
    }

    public void addVariableName(String NewName) {
        this.VariableNames.add(NewName);
    }

    private void setVaraibleValues(ArrayList<String> newVariableValues) {
        this.VariableValues = newVariableValues;
    }

    public List<String> getVariableValues() {
        return this.VariableValues;
    }

    public void addVariableValue(String VariableValue) {
        this.VariableValues.add(VariableValue);
    }
}