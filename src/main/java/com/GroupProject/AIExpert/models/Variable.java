package com.GroupProject.AIExpert.models;

public class Variable {
    public String name;
    public String value;

    public void setValue(String newValue) {
        this.value = newValue;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public String getValue() {
        return this.value;
    }

    public String getName() {
        return this.name;
    }
}