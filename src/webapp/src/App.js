import React from 'react';
import { CssBaseline } from '@material-ui/core';
import Dashboard from './pages/dashboard';
import { BrowserRouter } from 'react-router-dom';


function App() {
  return (
    <div>
      <BrowserRouter>
        <CssBaseline />
        <Dashboard />
      </BrowserRouter>
    </div>
  );
}

export default App;
