import React from 'react';
// import AccountBoxOutlinedIcon from '@material-ui/icons/AccountBoxOutlined';
import AnnouncementOutlinedIcon from '@material-ui/icons/AnnouncementOutlined';
import StorageOutlinedIcon from '@material-ui/icons/StorageOutlined';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import MenuBookIcon from '@material-ui/icons/MenuBook';




export const regularItems = [
    {
        "name":"Health Survey",
        "Icon": <LocalHospitalIcon/>,
        "linkto": "/survey"
    }
]

export const Adminitems = [
    // {
    //     "name":"Profile",
    //     "Icon": <AccountBoxOutlinedIcon/>,
    //     "linkto": "/profile"
    // },
    {
        "name":"Records",
        "Icon": <StorageOutlinedIcon/>,
        "linkto": "/records"
    },
    {
        "name":"All Alerts",
        "Icon": <AnnouncementOutlinedIcon/>,
        "linkto": "/alerts"
    },
    {
        "name":"Make Query",
        "Icon": <MenuBookIcon/>,
        "linkto": "/query"
    },
    {
        "name":"Knowledge base",
        "Icon": <MenuBookIcon/>,
        "linkto": "/knowledgebase"
    },
]