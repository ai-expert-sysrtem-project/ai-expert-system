import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import axios from "axios";



const Survey = () => {


    const useStyles = makeStyles(theme => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            width: 200,
          },
        container: {
            justifyContent: 'center',
            alignItems: 'center',
            // display: 'flex',
            // flexWrap: 'wrap',
        },
        container2: {
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            flexWrap: 'wrap',
        },
    }));
    const initialQueryResultState = {
        variableValues: [],
        variableNames : [],
        query: ""
    }
    const initialSurveyState = {
            temp: 0.0,
            cigarette_smoking: false,
            poor_hygiene_conditions: false,
            long_Term_Steroid_Usage: false,
            bone_Marrow_Transplant: false,
            asthma: false,
            stroke: false,
            malnourishment: false,
            bronchiectasis: false,
            lack_of_Exercise: false,
            chemical_Pollutants: false,
            cystic_Fibrosis: false
    }
    const [surveyobject,setsurveyobject] = useState(initialSurveyState);
    const [queryResult,setQueryResult] = useState(initialQueryResultState);
    const classes = useStyles();

    const handleChange = (event)=> {
        let {name,value} = event.target

        switch(name){
            case "temp":
                value = parseInt(value)
                break;
            default:
                value = value === "true"
        }

        setsurveyobject({ ...surveyobject, [name]: value})
       
        console.log(value)
        console.log(name)
        console.log(surveyobject)
    };

    const onSubmit = async () => { 
        let postendpoint = 'api/survey'
        if(validateFormdata(surveyobject))
        {
            try {

                const Response = await axios.post(postendpoint,surveyobject)
                setQueryResult(Response.data)
            }catch (error) {
                console.log(error)
            }
        }
    }

    const validateFormdata = (FormObject) => {
        let valid = true;

        Object.entries(FormObject).forEach( function([key,value]){
            switch(key) {
                case "temp":
                    if(value === null ) valid = false
                    break;
                default:
                    if(value === null ) valid = false
                    break;
            }
        })
        
        console.log("Valid Value")
        console.log(valid)
        return valid;
    }


    return (
        <Paper className={classes.container}>
                    <Typography className={classes.container}
                        >Please select the best responses below
                    </Typography>
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend">Enter the Temperature in Degrees fahrenheit</FormLabel>
                        <TextField
                        id="standard-basic"
                        name={"temp"}
                        type="number"
                        onChange={handleChange}
                        defaultValue={0.00}
                        className={classes.textField}
                        label="fahrenheit"
                        margin="normal"
                    />
                
                    <FormLabel component="legend">have you ever been exposed to Chemical Pollutants</FormLabel>
                    <RadioGroup 
                        value={surveyobject.chemical_Pollutants}
                        aria-label="Chemmical_pollution" 
                        name="chemical_Pollutants" 
                        onChange={handleChange} >
                        <FormControlLabel value={true} control={<Radio />}  label="Yes"  />
                        <FormControlLabel value={false} control={<Radio />} label="No"    />
                    </RadioGroup>

                    <FormLabel component="legend">Are you a Smoker?</FormLabel>
                    <RadioGroup  aria-label="cigerrette_smoker" name="cigarette_smoking" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />}  checked={surveyobject.cigarette_smoking} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.cigarette_smoking} label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">Have you ever been or have suffered from Malnourishment</FormLabel>
                    <RadioGroup  aria-label="malnourishment" name="malnourishment" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />}  checked={surveyobject.malnourishment} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.malnourishment} label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">how would you describe your hygienic conditions</FormLabel>
                    <RadioGroup  aria-label="hygene" name="poor_hygiene_conditions" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />}  checked={surveyobject.poor_hygiene_conditions} label="Excellent" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.poor_hygiene_conditions} label="Poor" />
                    </RadioGroup>

        
                    <FormLabel component="legend">Are you getting adequate Excerise?</FormLabel>
                    <RadioGroup  aria-label="Adequete excersise" name="lack_of_Exercise" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />} checked={surveyobject.lack_of_Exercise}  label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.lack_of_Exercise}  label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">Have you or anybody in your family ever suffered from a Stroke?</FormLabel>
                    <RadioGroup  aria-label="stroke" name="stroke" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />} checked={surveyobject.stroke} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.stroke} label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">Have you or anybody in your family ever suffered from Cystic Fibrosis?</FormLabel>
                    <RadioGroup  aria-label=" Cystic Fibrosis" name="cystic_Fibrosis" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />} checked={surveyobject.cystic_Fibrosis} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.cystic_Fibrosis} label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">Have you or anybody in your family ever suffered from Asthma?</FormLabel>
                    <RadioGroup aria-label="Asthma" name="asthma" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />} checked={surveyobject.asthma} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.asthma}  label="No" />
                    </RadioGroup>


                    <FormLabel component="legend">Have you or anybody in your family ever suffered from Bronchiectasis?</FormLabel>
                    <RadioGroup aria-label="Bronchiectasis" name="bronchiectasis" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />}  checked={surveyobject.bronchiectasis} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.bronchiectasis} label="No" />
                    </RadioGroup>


                    <FormLabel component="legend">Have you or anybody in your family ever been the recipient of a Bone Marrow Transplant?</FormLabel>
                    <RadioGroup aria-label="Bone Marrow Transplantsplant" name="bone_Marrow_Transplant" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />}  checked={surveyobject.bone_Marrow_Transplant}  label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.bone_Marrow_Transplant}  label="No" />
                    </RadioGroup>

                    <FormLabel component="legend">Have you or anybody in your family ever been a Long Term Steroid Usager?</FormLabel>
                    <RadioGroup aria-label="Long Term Steroid Usage" name="long_Term_Steroid_Usage" onChange={handleChange}>
                        <FormControlLabel value={true} control={<Radio />} checked={surveyobject.long_Term_Steroid_Usage} label="Yes" />
                        <FormControlLabel value={false} control={<Radio />} checked={!surveyobject.long_Term_Steroid_Usage} label="No" />
                    </RadioGroup>
                    <Button
                    variant="contained"
                    color="primary"
                    className={classes.container}
                    onClick={onSubmit}
                >
                    Submit
                </Button>
            </FormControl>
            {
                queryResult === initialQueryResultState 
                
                ? 
                <Paper className={classes.container}>
                    <Typography className={classes.container2}>
                    Submit a complete form to see results
                 </Typography>
                </Paper>
                :
            <>
            <Paper className={classes.container}>
                <Typography className={classes.container2}>
                    Results
                </Typography>
            </Paper>
            <Paper className={classes.container}>
                <Typography className={classes.container2}>
                   {queryResult.variableValues[0] * 100 % 100}%
                </Typography>
            </Paper>
            </>
            }
        </Paper >

    )
}


export default Survey


