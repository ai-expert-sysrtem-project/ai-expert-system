import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  tableWrapper: {
    maxHeight: 440,
    overflow: 'auto',
  },
});

export default function DataTable({ Headings, TableData }) {
  const columns = Headings.map((Heading, index) => (
    { id: index, label: Heading.name, align: "center" }
  ))
 
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <div className={classes.tableWrapper}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow selected>
              {columns.map((column, index) => (
                <TableCell
                  key={index}
                  align={"center"}
                // style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {TableData.map((data, index) => (
              <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                {Object.values(data).map((Value, index) => (
                  <TableCell key={index} align={"center"}>
                    {Value}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </Paper>
  );
}