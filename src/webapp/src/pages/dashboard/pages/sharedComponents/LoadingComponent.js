import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    loadingcircle: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(2),
        },
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: "10%"
      },
}));


const LoadingComponent = () => {
    const classes = useStyles();
    return (
        <div className={classes.loadingcircle}>
            <CircularProgress />
            <Typography variant="h5" component="h3">
                Retrieving Data
            </Typography>
        </div>
    )
}

export default LoadingComponent 