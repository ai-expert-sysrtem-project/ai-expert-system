import React, { useState,useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import DataTable from "../sharedComponents/dataTable";
import LoadingComponent from "../sharedComponents/LoadingComponent";
import axios from "axios";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
    },
    loadingcircle: {
        display: 'flex',
        '& > * + *': {
        marginLeft: theme.spacing(2),
        },
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: "10%"
      },
    alignItemsAndJustifyContent: {
        width: "100%",
        height: "100%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'inherit',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: "50%",
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
    },
    results: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: "10%"
    },
    button: {
        margin: theme.spacing(1),
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
}));



const Alerts = () => {
    const classes = useStyles();
    const [loading,setLoading] = useState(true)
    const [alerts, setAlerts] = useState([])


    useEffect( ()=> {
         async function fetchdata() {
             let response = await axios.get("api/alert")
           
            setAlerts(response.data)
         }
         setLoading(true)
         fetchdata()
         setLoading(false)
    },[])

    return (
        <div>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                    Alerts
                </Typography>
            </Paper>            
                {loading === true ? 
                    <LoadingComponent />
                 :
                <Paper>
                <DataTable
                    TableData={alerts}
                    Headings={Headings}
                />
                 </Paper>
                } 
           
        </div>
    )
}

const Headings =
    [
        {
            "name": "ID",
        },
        {
            "name": "Reciever",
        },
        {
            "name": "message",
        },
        {
            "name": "subject",
        }
    ]

export default Alerts