import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import axios from "axios";
import DataTable from "../sharedComponents/dataTable";
import LoadingComponent from '../sharedComponents/LoadingComponent';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
    },
    alignItemsAndJustifyContent: {
        width: "100%",
        height: "100%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'inherit',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: "50%",
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
    },
    results: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: "10%"
    },
    button: {
        margin: theme.spacing(1),
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
}));

const Records = () => {
    const classes = useStyles();
    const [loading, setLoading ] = useState(false)
    const [surveys, setSurveys] = useState([])

    useEffect( () => {
        async function fetchdata() {
            const response = await axios.get("api/survey")

           let data = response.data.map(val => (
               Object.values(val).map( val => val === undefined ? "no data" : String(val) )
           ))
            console.log(data)
            console.log(response.data)
            setSurveys(data)
        }
        setLoading(true)
        fetchdata()
        setLoading(false)

    },[])

    return (
        <div>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                    Survey Records
                </Typography>
            </Paper>

            {loading === true ?
                <LoadingComponent/>
            :
            <Paper>
                <DataTable
                    TableData={surveys}
                    Headings={Headings}
                    />
            </Paper>
            }
        </div>
    )
}


const Headings =
    [
        {
            "name": "ID",
        },
        {
            "name": "Temperature in celsius",
        },
        {
            "name": "cigarette smoking",
        },
        {
            "name": "poor hygiene conditions",
        },
        {
            "name": "long Term Steroid Usage",
        },
        {
            "name": "bone Marrow Transplant",
        },
        {
            "name": "asthma",
        },
        {
            "name": "stroke",
        },
        {
            "name": "malnourishment",
        },
        {
            "name": "bronchiectasis",
        },
        {
            "name": "lack of Exercise",
        },
        {
            "name": "chemical Pollutants",
        },
        {
            "name": "cystic Fibrosis",
        },
    ]

export default Records