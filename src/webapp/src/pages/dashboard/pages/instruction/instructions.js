import React from 'react'
// import { makeStyles } from '@material-ui/core/styles';
// import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles(theme => ({
  root: {
      padding: theme.spacing(3, 2),
  },
  alignItemsAndJustifyContent: {
      width: "100%",
      height: "100%",
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'inherit',
  },
  textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: "50%",
  },
  container: {
      display: 'flex',
      flexWrap: 'wrap',
  },
  results: {
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
      flexWrap: 'wrap',
      marginTop: "10%"
  },
  button: {
      margin: theme.spacing(1),
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
  },
  presetbuttons: {
      marginLeft: "10px",
      marginRight: "10px"
  }
}));




const Instruction = () => {
  const classes = useStyles();
    
    return (
      <Paper >
          <Paper variant="h1" component="h1" className={classes.container}>
            Prolog Rules and Predicates

          </Paper>
          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Jamaica',0.25).

          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('cigarette_smoking',0.3).
          </Paper>
          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Malnourishment',0.25).

          </Paper>
          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('poor_hygiene_conditions',0.3).
          </Paper>
          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Lack_of_Exercise',0.2).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Chemical_Pollutants',0.45).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Stroke',0.3).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
             risk_factor('Cystic_Fibrosis',0.2).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
             risk_factor('Asthma',0.54).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
             risk_factor('Bronchiectasis',0.5).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
             risk_factor('Bone_Marrow_Transplant',0.4).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_factor('Long_Term_Steroid_Usage',0.1).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            single_risk_score(Who):-risk_factor(Who,X),write(X).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_total([],0).
          </Paper>

          <Paper variant="h5" component="h3" className={classes.container}>
            risk_total([Risk|More],X) :- risk_total(More,Score), risk_factor(Risk,RiskScore),X is RiskScore + Score.
          </Paper>

          
  

      
      </Paper>
      
      
    
    );
}

export default Instruction