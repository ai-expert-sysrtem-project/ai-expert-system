import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from "axios";
import KnowbasedataTable from "../sharedComponents/KnowbasedataTable";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
    },
    alignItemsAndJustifyContent: {
        width: "100%",
        height: "100%",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'inherit',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: "50%",
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
    },
    results: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: "10%"
    },
    button: {
        margin: theme.spacing(1),
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    presetbuttons: {
        marginLeft: "10px",
        marginRight: "10px"
    }
}));



const KnowledgeBase = () => {
    
    const [query, setQuery] = useState()
    const classes = useStyles();
    const [tableObj,settableObj] = useState(null)

    const handleChange = (event) => {
        const { value } = event.target
        console.log(value);
        setQuery(value)
    }

    const handleSetQuery = (newQuery) => {
        setQuery(String(newQuery))
    }
    const onSubmit = async (event) => {
        event.preventDefault();
        await axios.post("api/query", query)
            .then((res) => {

                const  headingcount = res.data.variableNames.length
                const datacount = res.data.variableValues.length
                let newObj = []
                let arraypos = 0
                for(let x = 0 ;x<datacount/headingcount;++x){
                    for(let y=0; y<headingcount; ++y) {
                        if(headingcount>1)
                        newObj.push(
                            {
                                [res.data.variableNames[y]] : res.data.variableValues[arraypos],
                                [res.data.variableNames[++y]] : res.data.variableValues[++arraypos],
                            }
                        )
                        else{
                            newObj.push(
                                {
                                    [res.data.variableNames[y]] : res.data.variableValues[arraypos],
                                }
                            )
                        }

                        arraypos++
                    }
                }
                console.log(newObj);

                settableObj({...tableObj,
                                headings: res.data.variableNames.map(value => ({name : value})),
                                TableData: newObj
                            })
                // settableObj({...tableObj,TableData:res.data.variableValues})
                
            })
            .catch((err) => {
                console.log(err)
            })
    }

    return (
        <div>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                    Query the KnowledgeBase
                </Typography>

            </Paper>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                   Omit full stops( . )
                </Typography>

            </Paper>
            <Paper className={classes.container}>
                <TextField
                    id="standard-basic"
                    className={classes.textField}
                    // label="Enter your query"
                    value={query}
                    margin="normal"
                    onChange={handleChange}
                />
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.container}
                    onClick={onSubmit}
                >
                    Submit
                </Button>
            </Paper>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                    Presets
                </Typography>

            </Paper>
            <Paper className={classes.container}>
                <Typography variant="h5" component="h3">
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.presetbuttons}
                    onClick={handleSetQuery.bind(this,"risk_factor(Risks,Chance)")}
                >
                    risk_factor(Risks,Chance)
                </Button>
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.presetbuttons}
                    onClick={handleSetQuery.bind(this,"risk_factor('Asthma',Chance)")}
                >
                    risk_factor(Asthma,Chance)
                </Button>
                </Typography>

            </Paper>
            
            <Paper className={classes.results}>
                {tableObj === null
                    ?
                    <Typography variant="h6" component="h3">
                        Run a Query to view results
                    </Typography>
                    :
                    tableObj.headings === undefined || tableObj.headings.length < 1
                        ?
                        <Typography variant="h6" component="h3">
                            No Results
                        </Typography>
                        :
                        <KnowbasedataTable
                            Headings={tableObj.headings}
                            TableData={tableObj.TableData}
                        />
                }

            </Paper>


        </div>
    );
}

export default KnowledgeBase