import knowledgebase from "./pages/instruction";
import Survey from "./pages/survey";
import Profile from "./pages/profile";
import Records from "./pages/records";
import Alerts  from "./pages/alerts";
// import Login from "./pages/login";
import Query from "./pages/knowledge_base.js";

export const DashboardRoutes = [
    {
        "path": "/knowledgebase",
        "component": knowledgebase
    },
    {
        "path": "/survey",
        "component": Survey
    },
    {
        "path": "/profile",
        "component": Profile
    },
    {
        "path": "/records",
        "component": Records
    },
    {
        "path": "/alerts",
        "component": Alerts
    },
    // {
    //     "path": "/login",
    //     "component": Login
    // },
    {
        "path": "/query",
        "component": Query
    },
]