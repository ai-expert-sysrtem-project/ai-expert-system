import React from 'react'
import {Route, Switch } from 'react-router-dom';
import {DashboardRoutes} from "./dashboardroutes";

const Content = () => {
   
    return (
         <Switch>
           {
             DashboardRoutes.map(route=>(
             <Route 
              key={route.path}
              exact={true}
              path={route.path}
              component={route.component}
             />
             
             ))
           }
         </Switch>
    )
}

export default Content