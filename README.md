## Hosting
- Provider - Heroku
- Production URL - [https://pneumonia-expert-production.herokuapp.com/](https://pneumonia-expert-production.herokuapp.com/)
- Staging URL - TODO


## Tech Stack

### Java - SpringBoot
Used to create Restful API endpoints allowing the UI to perform CRUD operation on database

### React.js
Used to Create A Single Page Application.
ReactRouter used to to allow url navigation to pages
Axio used to make Restful HTTP Request to ServerSide APIs

### MySQL
TODO

### Prolog
TODO

## Tools

- Maven - TODO
- Java  - TODO
- jre   - TODO
- Node  - TODO
- npm   - TODO



## To start this app

### `Using Maven - version 3.6.1`
- run 'gitclone https://gitlab.com/ai-expert-sysrtem-project/ai-expert-system.git' in terminal
- install mvn
- Run mvn Spring-boot:run
- open a browser and go to the url [http://localhost:8090](http://localhost:8090)

### `Using Jar File`

- Download the Jar file in build folder, or clone the latest Branch and run mvn -B package
- open the terminal in the folder where you downloaded the Jar File
- Java -jar jarnamehere.jar (replace jarnamehere with fike name)
- open a browser and go to the url [http://localhost:8090](http://localhost:8090)


